/// <reference path="../common.d.ts" />

class Room {
	private id: number;
	private name: string;
	private devices: Object;
	private img: string;
	private power: boolean;

	constructor(id: number, name: string, img?: string) {
		this.id = id;
		this.name = name;
		this.img = img;
		this.power = true;
		this.devices = new Object();
	}

	public getId = () => {
		return this.id;
	}

	public getName = () => {
		return this.name;
	}

	public getDevices = () => {
		return this.devices;
	}

	public getImg = () => {
		return this.img;
	}

	public getPower = () => {
		return this.power;
	}
	
	/**
	 * Odznacza wszystkie zapisane urzadzenia 
	 */
	public unAssignAll = () => {
		for (var id in this.devices) {
			this.devices[id].unAssign();
		}

	}
	/**
	 * Usuwa wszystkie zapisane urządzenia
	 */
	public removeAll = () => {
		for (var id in this.devices) {
			delete this.devices[id];
		}
	}
	/**
	 * Przełącza wszystkie urządzenia w zależności od this.power
	 */
	public turnAll = () => {
		if (this.power) 
			this.powerOnDevices();
		else 
			this.powerOffDevices();
	}
	/**
	 * Włącza wszystkie urządzenia
	 */
	private powerOnDevices = () => {
		for (var id in this.devices) {
			this.devices[id].powerOn();
		}
	}
	/**
	 * Wyłącza wszystkie urządzenia
	 */
	private powerOffDevices = () => {
		for (var id in this.devices) {
			this.devices[id].powerOff();
		}
	}
	/**
	 * Dodaje urządzenie do pokoju
	 */
	public addDevice = (device: Device) => {
		if (!device.checkAssign()) {
			this.devices[device.getUuid()] = device;
			device.assign();
		}
		else alert('Już jest przypisany');
	}
	/**
	 * Usuwa urządzenie z pokoju
	 */
	public removeDevice = (device: Device) => {
		delete this.devices[device.getUuid()];
		device.unAssign();
	}
}




class RoomsService {
	private DeviceService: DevicesService;

	private roomList: Object = new Object();

	constructor(DeviceService: DevicesService) {
		this.DeviceService = DeviceService;
	}
	
	/**
	 * Zwraca wszystkie aktualne pokoje
	 */
	public getRooms = () => {
		return this.roomList;
	}

	/**
	 * Dodaje pokój
	 */
	public addRoom = (name: string, img?: string) => {
		if (this.roomList == null) {
			this.roomList[0] = new Room(0, name, img)
		}
		else {
			var index = Object.keys(this.roomList).length;
			if (!this.roomList.hasOwnProperty(index.toString())) {
				this.roomList[index] = new Room(index, name, img);
			}
			else alert("Już istnieje pokój o takim id");
		}
	}
	/**
	 * Usuwa pokój
	 */
	public removeRoom = (id: number) => {
		if (!this.roomList.hasOwnProperty(id.toString())) {
			this.roomList[id].unAssignAll();
			this.roomList[id].removeAll();
			delete this.roomList[id];
		}
		else alert("Nie istnieje pokój o takim id");
	}
}

lightApp.factory('RoomsService', ['DevicesService', (DevicesService) => new RoomsService(DevicesService)]);