/// <reference path="../common.d.ts" />
/**
 * Obiekt urządzenia
 */
class Device {
	private deviceHash: string;
	private uuid: string;
	private name: string;
	private rsii: number;
	private power: boolean;
	private assigned: boolean; //określa czy dane urzadzenie jest juz przypisane

	constructor(data: IDeviceData) {

		this.deviceHash = data.deviceHash;
		this.uuid = data.uuid;
		this.name = data.name;
		this.rsii = data.rsii;
		this.power = data.power;
		this.assigned = data.assigned;
	}

	public checkAssign = (): boolean => {
		return this.assigned;
	}

	public assign = () => {
		this.assigned = true;
	}

	public unAssign = () => {
		this.assigned = false;
	}

	public getUuid = () => {
		return this.uuid;
	}

	public getName = () => {
		return this.name;
	}

	public getRssi = () => {
		return this.rsii;
	}

	public getPower = () => {
		return this.power;
	}

	public getDeviceHash = () => {
		return this.deviceHash;
	}

	/**
	 * Wyłącza światło
	 */
	public powerOff = () => {

		this.power = false;
		alert("Power OFF");
	}
	/**
	 * Włącza światło
	 */
	public powerOn = () => {
		this.power = true;
		alert("Power ON");
	}
}

/**
 * Obiekt światła
 */
class LightDevice extends Device {

	private brightness: number;

	constructor(data: ILightDeviceData) {
		super(data);
		this.brightness = data.brightness;
	}

	public setBrightness = (brightness: number) => {
		this.brightness = brightness;
	}


}


class DevicesService {
	
	//
	private http: ng.IHttpService;
	// Przypisane urządzenia
	private deviceList: Object = null;
	// Nie przypisane urządzenia
	private freeDeviceList: Object = null;

	constructor($http: ng.IHttpService) {
		this.http = $http;
	}
	/**
	 * Ładuje dane do this.devicelist
	 */
	private loadData = () => new Promise((onSucces, onError) => {
		this.http.get('../../light.json').success((data: Object) => {
			if (!this.deviceList) {
				this.deviceList = new Object();
				var keys = Object.keys(data);
				for (var index = 0, len = keys.length; index < len; index++) {
					var key = keys[index];
					var value: ILightDeviceData = data[key];
					this.deviceList[key] = new Device(value);
				}
			}

			onSucces(this.deviceList);

		}).error((error) => {
			onError();
		});
	});
		
	
	
	/**
	 * Pobiera dane na temat urządzeń
	 * 
	 *  */
	public getDevices = (): Promise<Object> => {
		return new Promise((onSucces, onError) => {
			if (!this.deviceList) {
				this.loadData().then(() => { onSucces(this.deviceList); });
			}
			else {
				onSucces(this.deviceList);
			}


		})
	}
	/**
	 * Pobiera urządzenia które nie są przypisane do żadnego pomieszczenia
	 */
	private getUnassignedDevicesList = (): Object => {
		var result = new Object();
		for (var id in this.deviceList) {
			if (!this.deviceList[id].checkAssign()) {
				result[id] = this.deviceList[id];
			}
		}
		return Object.keys(result).length > 0 ? result : null;

	}
	/**
	 * Jak wyżej, publiczna, dodatkowo sprawdza czy było już pobranie danych
	 */
	public getUnassignedDevices = (): Promise<Object> => {
		return new Promise((onSucces, onError) => {
			if (!this.deviceList) {
				this.loadData().then(() => {
					onSucces(this.getUnassignedDevicesList());
				});
			}
			else {
				onSucces(this.getUnassignedDevicesList())
			}


		})
	}
	
	/**
	 * Poszukuję nieprzypisanych urządzeń
	 */
	public searchDevices = () => {
		alert("searchDevices")
	}
	/**
	 * Sprawdza czy dane urządzenie należy do nas
	 */
	public checkDevice = (uuid: string): boolean => {
		return this.deviceList.hasOwnProperty(uuid);
	}
	
	/**
	 * Ustawia światła
	 */
	public setBrightness = (uuid: string, brightness: number): void => {
		alert("setBrightness");
	}
	/**
	 * Włącz światło dla uuid
	 */
	public turnOn = (uuid: string) => {
		alert("turnOn");
	}
	/**
	 * Wyłącz światło dla uuid
	 */
	public turnOff = (uuid: string) => {
		alert("turnOff");
	}
	/**
	 * Przypisuje urządzenie do użytkownika
	 */

	public assignDevice = (uuid: string) => {
		alert("assignDevice");
	}

	/**
	 * Dodaje urządzenie do listy
	 */
	private addDeviceToList = (device: Device) => {
		this.checkDevice(device.getUuid()) ? this.deviceList[device.getUuid()] = device : alert('już jest');
	}
	/**
	 * Usuwa urządzenie z listy
	 */
	private removeDeviceFromList = (device: Device) => {
		this.checkDevice(device.getUuid()) ? this.deviceList[device.getUuid()] = undefined : alert('nie istnieje');
	}


}
lightApp.factory('DevicesService', ['$http', ($http) => new DevicesService($http)]);