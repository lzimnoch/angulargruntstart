/// <reference path="../../typings/tsd.d.ts" />
/// <reference path="services/DevicesService.ts" />
/// <reference path="services/RoomsService.ts" />



/// <reference path="main.ts" />
interface IDeviceData {
	deviceHash: string;
	uuid: string;
	name: string;
	rsii: number;
	power: boolean;
	assigned: boolean;
}

interface ILightDeviceData extends IDeviceData {
	brightness: number;
}