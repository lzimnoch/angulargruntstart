/// <reference path="common.d.ts" />

var lightApp = angular.module('lightApp', ["ngRoute"]);

lightApp.config(['$routeProvider',
  ($routeProvider) => {
    $routeProvider.
      when('/test', { 
        templateUrl: 'ts/views/roomList.html',
        controller: 'testcont'
      }).
      otherwise({
        redirectTo: '/test'
      });
  }]);