var concat = {
  options: {
    stripBanners: true
  },
  js: {
    src: [
      "src/ts/controllers.js"
    ],
    dest: 'dist/release/UP-src.js',
    nonull: true
  },

  css: {
    src: [
      'src/leaflet/leaflet.css',
      'src/lib/**/*.css',
      'src/sass/styles.css',
    ],
    dest: 'dist/release/css/UP-src.css',
    nonull: true
  }
}
module.exports = function (grunt) {
  grunt.config.set('concat', concat);
}
