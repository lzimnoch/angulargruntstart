'use strict';
module.exports = function (grunt) {
  require('load-grunt-tasks')(grunt);
  require('time-grunt')(grunt);
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    tsd: {
      refresh: {
        options: {
          command: 'reinstall',
          latest: false,
          config: 'tsd.json'
        }
      }
    },
     processhtml: {
      options: {
        data: {
        }
      },
      release: {
        files: {
          'dist/release/main.html': ['src/main.html']
        }
      }
    },
    // Bower plugin
    "bower-install-simple": {
      options: {
        color: true,
        directory: 'external',
        update: true,
        command: "update"
      },
      dev: {
      }
    }
  });
  grunt.loadTasks('tasks');
  
  grunt.registerTask('styles', [
  ]);

  grunt.registerTask('scripts', [
    'ts',
  ]);
  
  grunt.registerTask('external', [
    'bower-install-simple',
    'copy:external',
  ]);
  
  grunt.registerTask('default', [
    'dev',
  ]);
  
  grunt.registerTask('dev', [
    'external',
    'tsd',
    'styles',
    'scripts'
  ]);
  
  grunt.registerTask('release', [
    'dev',
    'copy:release',
    'concat:js',
    'concat:css',
    'processhtml:release',
    'cssmin',
    'uglify'
  ]);
  
  grunt.registerTask('deploy', [
    'release',
    'copy:deploy'
  ]);
  
}
